#!/usr/bin/env python

import json
import sys
# pip install --user semantic_version
from semantic_version import Version
import collections

data = json.load(sys.stdin)

docs = data["response"]["docs"]

#print json.dumps(docs, indent=2)
def info(msg):
  print >> sys.stderr, msg

info('Found ' + str(len(docs)) + ' artifacts')

# Some magic hacks to get semantic_version to like the versions more
def v(doc):
  return doc["v"]. \
    replace("alpha1", "0.alpha-1"). \
    replace("alpha2", "0.alpha-2")

found = {}
for doc in docs:
  key = doc["g"] + ":" + doc["a"]
  prev = found.get(key)
  if prev == None:
    found[key] = doc
#    print "First version of " + key + " => " + str(Version.coerce(doc["v"]))
    continue

  oldV = Version.coerce(v(prev))
#  print "oldV=" + str(oldV)
  newV = Version.coerce(v(doc))
#  print "newV=" + str(newV)

  if newV > oldV:
#    print "newer version of " + key + " => " + str(newV)
    found[key] = doc
#  else:
#    print "old   version of " + key + " => " + str(newV)

found = collections.OrderedDict(sorted(found.items()))

print "<pluginManagement>"
print "<plugins>"
for key, p in found.iteritems():
  print """    <plugin>
      <groupId>%(g)s</groupId>
      <artifactId>%(a)s</artifactId>
      <version>%(v)s</version>
    </plugin>""" % p
print "  </plugins>"
print "</pluginManagement>"
